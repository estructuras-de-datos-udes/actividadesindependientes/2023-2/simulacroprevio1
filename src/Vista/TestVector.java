/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.VectorEnteros;

/**
 *
 * @author madar
 */
public class TestVector {
    public static void main(String[] args) {
        Consola teclado=new Consola();
        int n=teclado.leerEntero("Digite cuantos números desea generar para vector1:");
        VectorEnteros v1=new VectorEnteros(n);
        n=teclado.leerEntero("Digite cuantos números desea generar para vector2:");
        VectorEnteros v2=new VectorEnteros(n);
        teclado.imprimir("Vector v1:"+v1.toString());
        teclado.imprimir("Vector v2:"+v2.toString());
        /**
         * Debe implementar los métodos ya que le generar error
         * por devolver objetos "null"
         */
        VectorEnteros v3=v1.getUnion(v2);
        VectorEnteros v4=v1.getUnion(v2);
        teclado.imprimir("v1 U v2:"+v3.toString());
        teclado.imprimir("v1  ∩ v2:"+v4.toString());
        
    }
}
