/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class VectorEnteros {

    int vector[];

    /**
     * Crea un vector de tamaño n, con números aleatorios
     *
     * @param n
     */
    public VectorEnteros(int n) {

    }

    /**
     * Obtiene la unión de conjuntos de dos vectores
     *
     * @param vector2 vector con el que se va a realizar la unión
     * @return un nuevo vector con la unión.
     */
    public VectorEnteros getUnion(VectorEnteros vector2) {
        return null;
    }

    /**
     * Obtiene la intersección de conjuntos de dos vectores
     *
     * @param vector2 vector con el que se va a realizar la unión
     * @return un nuevo vector con la intersección.
     */
    public VectorEnteros getInterseccion(VectorEnteros vector2) {
        return null;
    }

    @Override
    public String toString() {
        if (this.vector == null) {
            return "Vector no contiene elementos";
        }
        String msg = "";
        for (int i : vector) {
            msg += i + "\t";

        }
        return msg;
    }

}
